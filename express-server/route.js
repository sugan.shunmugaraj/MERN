// ./express-server/routes/product.server.route.js
import express from 'express';

import * as projectController from './controllers/projects.server.controller';



var multer = require("multer");

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/')
    },
    filename: function(req, file, cb) {
        console.log('File Name', file);
        cb(null, Date.now() + '.jpg')
    }
});

var upload = multer({ storage: storage });

// get an instance of express router
const router = express.Router();


//New Products
router.route('/getprojects').get(projectController.getProjects);
router.route('/saveproject').post(projectController.addProject);
// router.route('/updateproject').put(projectController.updateProject);
router.route('/getprojectById/:id').get(projectController.getProjectById)
    // router.route('/deleteproject/:id').delete(projectController.deleteProject);



//New Products
router.route('/getEmployees').get(projectController.getEmployees);
router.route('/saveemployee').post(projectController.addEmployee);
// router.route('/updateproject').put(projectController.updateProject);
// router.route('/getprojectById/:id').get(projectController.getProjectById)
// router.route('/deleteproject/:id').delete(projectController.deleteProject);

// file upload
router.post('/upload', upload.array("myfile[]", 12), function(req, res, next) {
    return res.send({
        success: true,
        file: req.files
    });
});

export default router;