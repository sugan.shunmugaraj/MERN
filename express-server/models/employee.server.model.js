import mongoose from 'mongoose';

var Schema = mongoose.Schema;

var employeeSchema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    name: String,
    email: String,
    EmpId: Number,
    Skills: Array
});

export default mongoose.model('Employees', employeeSchema);