import mongoose from 'mongoose';
import Employees from './employee.server.model';

var Schema = mongoose.Schema;

var projectSchema = mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    },
    projectName: String,
    projectlogo: { type: String, default: 'https://attachedlanguage.com/wp-content/uploads/2017/09/dummy-logo.png' },
    projectDescription: String,
    projectMembers: [{ type: Schema.Types.ObjectId, ref: 'Employees' }]
});

export default mongoose.model('Projects', projectSchema);