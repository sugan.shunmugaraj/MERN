// ./express-server/controllers/product.server.controller.js
import mongoose from 'mongoose';

//import models
import Projects from '../models/projects.server.model';
import Employees from '../models/employee.server.model';


export const getProjects = (req, res) => {
    Projects.find().populate('projectMembers').exec((err, projects) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in Fetching projects' });
        }
        return res.json({ 'success': true, 'message': 'Projects fetched successfully', projects });
    });
}


export const addProject = (req, res) => {
    console.log(req.body);
    const newProject = new Projects(req.body);
    newProject.save((err, project) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error tod' });
        }
        return res.json({ 'success': true, 'message': 'Products added successfully', project });
    })
}

export const getEmployees = (req, res) => {
    Employees.find().exec((err, employees) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error in Fetching employees' });
        }
        return res.json({ 'success': true, 'message': 'employees fetched successfully', employees });
    });
}

export const addEmployee = (req, res) => {
    console.log(req.body);
    const newEmployees = new Employees(req.body);
    newEmployees.save((err, employee) => {
        if (err) {
            return res.json({ 'success': false, 'message': 'Some Error tod' });
        }
        return res.json({ 'success': true, 'message': 'Products added successfully', employee });
    })
}

// export const updateProducts = (req, res) => {
//     console.log(req.body, req.body._id)
//     Products.findOneAndUpdate({ _id: req.body._id }, req.body, { new: true }, (err, product) => {
//         if (err) {
//             return res.json({ 'success': false, 'message': 'Some Error', 'error': err });
//         }
//         console.log(product);
//         return res.json({ 'success': true, 'message': 'Products Updated Successfully', product });
//     })
// }

export const getProjectById = (req, res) => {
    console.log(req.params.id)

    Projects.find({ _id: req.params.id }).populate('projectMembers').exec((err, project) => {

        if (err) {
            return res.json({ 'success': false, 'message': 'Some Errord' });
        }
        if (project.length) {
            return res.json({ 'success': true, 'message': 'project fetched by id successfully', project });
        } else {
            return res.json({ 'success': false, 'message': 'project with the given id not found' });
        }
    })
}

// export const deleteProducts = (req, res) => {
//     Products.findByIdAndRemove(req.params.id, (err, product) => {
//         if (err) {
//             return res.json({ 'success': false, 'message': 'Some Error' });
//         }
//         return res.json({ 'success': true, 'message': 'Products Deleted Successfully', product });
//     })
// }