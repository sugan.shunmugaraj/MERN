import React, { Component } from 'react';
import axios from 'axios';
import { Form, Text, TextArea , Select } from 'react-form';

class CreateProject extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            FormValue: '',
            projectMembers: ''
                    };
    }

    SubmitForm(val) {
       axios.post("http://localhost:4000/route/saveproject/" , val )
        .then(res => {
            console.log(res);
        })

    }

    componentWillMount() {
        axios.get("http://localhost:4000/route/getEmployees/")
        .then(res => {
            for(let i = 0; i<res.data.employees.length; i++){
                let EmpVal = {
                    label: res.data.employees[i].name,
                    value: res.data.employees[i]._id
                }
                this.state.projectMembers =  [...this.state.projectMembers , EmpVal]
            }
        })
     }

    componentDidMount() { }


    render() {
        return (
            <div className="formcontainer">
                <Form onSubmit={val => this.SubmitForm(val)}>
                    {formApi => (
                        <div>

                            <form onSubmit={formApi.submitForm} id="dynamic-form">
                                <ul className="projectform">
                                    <li>
                                        <label htmlFor="projectName">Project Name</label>
                                        <Text field="projectName" className="formField" id="projectName" />
                                    </li>
                                    <li>
                                        <label htmlFor="projectName">Project Name</label>
                                        <Text field="projectName" className="formField" id="projectName" />
                                    </li>
                                    <li>
                                        <label htmlFor="projectDescription">Project Description</label>
                                        <TextArea field="projectDescription" className="formField" id="projectDescription" />
                                    </li>
                                    <li>
                                        <button onClick={() => formApi.addValue('projectMembers', '')} type="button" className="mb-4 mr-4 btn btn-success">Add Member</button>

                                    </li>
                                    <li>
                                        {formApi.values.projectMembers && formApi.values.projectMembers.map((projectMember, i) => (
                                            <div className="memberGroup" key={`projectMembers${i}`}>
                                                <label htmlFor={`projectMembers-name-${i}`}>Member Name</label>
                                                {/* <Text field={['projectMembers', i]} id={`projectMember-name-${i}`} /> */}
                                                <Select field={['projectMembers', i]} className="formField" id={`projectMember-name-${i}`} options={this.state.projectMembers}  />
                                                <button onClick={() => formApi.removeValue('projectMembers', i)} type="button" className="removebtn">Remove</button>
                                            </div>
                                        ))}
                                    </li>
                                </ul>
                                <button type="submit" className="mb-4 btn btn-primary">Submit</button>
                            </form>
                        </div>
                    )}
                </Form>
            </div>
        );
    }


}
export default CreateProject;