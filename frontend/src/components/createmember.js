import React, { Component } from 'react';
import axios from 'axios';
import { Form, Text,  } from 'react-form';
import { Redirect } from 'react-router-dom'
class CreateMember extends Component {

    constructor(props) {
        super(props);
        this.state = { 
            FormValue: '',
            projectMembers: ''
                    };
    }

    SubmitForm(val) {
        axios.post("http://localhost:4000/route/saveemployee/" , val )
        .then(res => {
            console.log(res)
        })
        alert('saved Sucessfully');
        <Redirect to="/" />      
    }

    componentWillMount() {
        
     }

    componentDidMount() { }


    render() {
        return (
            <div className="formcontainer">
                <Form onSubmit={val => this.SubmitForm(val)}>
                    {formApi => (
                        <div>
                            <form onSubmit={formApi.submitForm} id="dynamic-form">
                                <ul className="projectform">
                                    <li>
                                        <label htmlFor="name">Employee Name</label>
                                        <Text field="name" className="formField" id="name" />
                                    </li>
                                    <li>
                                        <label htmlFor="email">Employee Email</label>
                                        <Text field="email" className="formField" id="email" />
                                    </li>
                                    <li>
                                        <label htmlFor="EmpId">Employee Id</label>
                                        <Text field="EmpId" type="number" className="formField" id="EmpId" />
                                    </li>
                                    <li>
                                        <button onClick={() => formApi.addValue('Skills', '')} type="button" className="mb-4 mr-4 btn btn-success">Add Skills</button>

                                    </li>
                                    <li>
                                        {formApi.values.Skills && formApi.values.Skills.map((Skill, i) => (
                                            <div className="memberGroup" key={`Skills${i}`}>
                                                <label htmlFor={`Skills-name-${i}`}>Skills</label>
                                                <Text className="formField skilltext" field={['Skills', i]} id={`Skill-name-${i}`} />
                                                {/* <Select field={['projectMembers', i]} className="formField" id={`projectMember-name-${i}`} options={statusOptions}  /> */}
                                                <button onClick={() => formApi.removeValue('Skills', i)} type="button" className="removebtn">Remove</button>
                                            </div>
                                        ))}
                                    </li>
                                </ul>
                                <button type="submit" className="mb-4 btn btn-primary">Submit</button>
                            </form>
                        </div>
                    )}
                </Form>
            </div>
        );
    }


}
export default CreateMember;