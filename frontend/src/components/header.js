import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => {
    return(
        <header>
            <div className="flexbox-container">
                <Link to="/" className="logo">
                    <span></span>
                </Link>
                <nav className="MenuLink">
                    <Link to="/">Projects List</Link>
                    <Link to="/createproject">Create Project</Link>
                    <Link to="/createmember">Add Employee</Link>                    
                </nav>
            </div>
        </header>
    )
}

export default Header;