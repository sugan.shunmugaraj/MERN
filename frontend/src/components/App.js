import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom';


// COMPONENTS
import Header from "./header";
import Projects from "./projects";
import Team from "./projectdetail";
import CreateProject from "./createproject";
import CreateMember from "./createmember";



class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <Header/>
          <Route exact path="/" component={Projects}></Route>
          <Route exact path="/projectdetail/:id" component={Team}></Route>
          <Route exact path="/createproject" component={CreateProject}></Route>
          <Route exact path="/createmember" component={CreateMember}></Route>                  
          {/* <Footer/> */}
        </div>
      </BrowserRouter>
    );
  }
}


export default App;
