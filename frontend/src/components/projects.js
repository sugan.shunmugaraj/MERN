import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Reveal from 'react-reveal'; // REACT REVEAL
import 'animate.css/animate.css'; //  REACT REVEAL
import axios from 'axios';

class Projects extends Component {
    constructor(props) {
        super(props)

        this.state = {
            teams: [],
            filtered: [],
            keyword: ''
        }
    }

    componentDidMount() {
        axios.get("http://localhost:4000/route/getprojects")
        .then(res => {
            console.log(res.data.projects);
            this.setState({ teams: res.data.projects, filtered: res.data.projects });
        })
    }

    searchTeam(event) {
        const keyword = event.target.value;
        if (keyword !== '') {
            const list = this.state.teams.filter((item) => {
                return item.projectName.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
            })
            this.setState({
                filtered: list,
                keyword
            })
        } else {
            this.setState({
                filtered: this.state.teams,
                keyword
            })
        }
    }


    renderList({ filtered }) {
        return filtered.map((item) => {
            return (
                <Reveal key={item._id} effect="animated fadeInUp" className={`item ${item.type}`}>
                <Link to={`/projectdetail/${item._id}`} key={item._id} className="team_item">
                    <img alt={item.name} src={`${item.projectlogo}`} />                  
                </Link>
                </Reveal>
            )
        })

    }  

    render() {
        return (
            <div className="teams_component">
                <div className="teams_input">
                    <input type="text"
                        value={this.state.keyword}
                        onChange={e => this.searchTeam(e)}
                        placeholder="Search for a Project" />
                </div>
                <div className="teams_container">
                {this.renderList(this.state)}
                 
                </div>
            </div>
        )
    }


}

export default Projects;