import React,{ Component } from 'react';
import axios from 'axios';


class Team extends Component{
    constructor(props){
        super(props)

        this.state = {
            data:[]
        }
    }

    componentDidMount(){
    //    fetch(`${URL_TEAMS}?name=${this.props.match.params.id}`, { method: 'GET'})
    //     .then(response => response.json())
    //     .then(json => {
    //         this.setState({data:json})
    //     })

    axios.get("http://localhost:4000/route/getprojectById/" + this.props.match.params.id )
        .then(res => {
            
            const activeproject = res.data.project;
            // this.setState({ teams: res.data.projects, filtered: res.data.projects })
                //   const persons = res.data;
                //   this.setState({ persons });
                console.log(activeproject);

                this.setState({data:activeproject})
        })
    }

    renderSkills(skills){
        return skills.map((item)=>{
            return(
                <label  key={item} className="skills">{item}</label>
            )
        })

    }

    renderEmployee(squad){
        return squad.map((item)=>{
            return(
                <div key={item._id} className="item player_wrapper">
                    <img alt={item.name} src={`/images/avatar.png`}/>
                    <h4>{item.name}</h4>
                    <div className="node">
                        <span>Emp Id:</span>  {item.EmpId}
                    </div>
                    <div className="node">
                        <span>Email: </span>{item.email}
                    </div>
                    <div className="node">
                        <span>Skills:</span>
                        {this.renderSkills(item.Skills)}
                        
                    </div>
                </div>
            )
        })
    } 

    renderData({data}){
        if(data){
            return data.map((item)=>{
                return (
                    <div key={item._id} className="team_data_wrapper">
                        <div className="left">
                            <img alt={item.name} src={`${item.projectlogo}`}/>
                            {/* <img alt={item.name} src={`/images/teams/${item.projectlogo}`}/> */}
                            
                        </div>
                        <div className="right">
                            <h1>{item.projectName}</h1>
                            <p>{item.projectDescription}</p>
                            <hr/>
                            <div className="squad">
                                {this.renderEmployee(item.projectMembers)}
                            </div>
                        </div>
                    </div>
                )
            })
    }
    }

    
     render(){
        return(
            <div className="team_data">
                {this.renderData(this.state)}
            </div>
        )
    }


}
export default Team;